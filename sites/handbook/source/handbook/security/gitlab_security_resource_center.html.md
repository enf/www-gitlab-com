---
layout: handbook-page-toc
title: "GitLab Security Resource Center"
description: "Provides an aggregated listing of popular and important links and information for GitLab's customers and prospects."
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Commonly requested resources

### Contacting GitLab for reporting security issues

* [Reporting Abuse](/handbook/security/security-operations/trustandsafety/abuse-on-gitlab-com.html)
* [Coordinated Disclosure Process](/security/disclosure/)
* [HackerOne Reporting Process](/handbook/security/security-engineering/application-security/runbooks/hackerone-process.html)


### GitLab's Customer Assurance Package (CAP)

Our Customer Assurance Package contains documents such as our SOC2 report, ISO 27001 certificate, penetration test executive summary, and pre-filled CAIQ and SIG questionnaires, among many other documents. Please see our [CAP page](/security/cap/) to request the package.

### GitLab's Trust Center

Our [Trust Center](/security/) outlines the various compliance and assurance credentials that GitLab maintains. This page also contains links to important security, legal & privacy, and availability resources, such as an [overview of our security practices](/handbook/security/#security-practices), our [Environmental, Social, and Governance strategy](/handbook/legal/ESG/), and our [production architecture](/handbook/engineering/infrastructure/production/architecture/).

## Frequently asked questions

The following links contain frequently asked security, legal & privacy, and availability questions.

* [Security FAQs](/security/faq/)
* [Legal & Privacy FAQs](/privacy/)
* [Availability FAQs](/handbook/engineering/infrastructure/faq/)

## Control topics

### Table of contents

| [Acceptable use](/handbook/security/gitlab_security_resource_center.html#acceptable-use) | [Access management](/handbook/security/gitlab_security_resource_center.html#access-management) | [Business continuity](/handbook/security/gitlab_security_resource_center.html#business-continuity) | [Cryptography](/handbook/security/gitlab_security_resource_center.html#cryptography) | [Data classification](/handbook/security/gitlab_security_resource_center.html#data-classification) 
| [Disaster recovery](/handbook/security/gitlab_security_resource_center.html#disaster-recovery) | [Endpoint management](/handbook/security/gitlab_security_resource_center.html#endpoint-management) | [Hardening](/handbook/security/gitlab_security_resource_center.html#gitlabs-gitlabcom-hardening-techniques) | [Incident response and communication](/handbook/security/gitlab_security_resource_center.html#incident-response-and-communication) | [Independent assurance](/handbook/security/gitlab_security_resource_center.html#independent-assurance)
| [Logging and monitoring](/handbook/security/gitlab_security_resource_center.html#logging-and-monitoring) | [Network security](/handbook/security/gitlab_security_resource_center.html#network-security) | [Privacy](/handbook/security/gitlab_security_resource_center.html#privacy) | [Security awareness](/handbook/security/gitlab_security_resource_center.html#security-awareness) | [Third party risk management](/handbook/security/gitlab_security_resource_center.html#third-party-risk-management) 
| [Threat modeling](/handbook/security/gitlab_security_resource_center.html#threat-modeling) | [Vulnerability management](/handbook/security/gitlab_security_resource_center.html#vulnerability-management) |

### Acceptable use

* [Internal acceptable use policy](/handbook/people-group/acceptable-use-policy/)
* [GitLab's terms of use](/terms/)

### Access management

* [Access Management Policy](/handbook/security/access-management-policy.html)
* [Access Review Procedure](/handbook/security/security-assurance/security-compliance/access-reviews.html)
* [Access Request process](/handbook/business-technology/end-user-services/onboarding-access-requests/access-requests/)

### Business continuity

* [Business Continuity Plan](/handbook/business-technology/gitlab-business-continuity-plan/)
* [Business Impact Analysis](/handbook/security/security-assurance/security-risk/storm-program/business-impact-analysis.html)
* [Information System Contingency Plan](/handbook/security/Information-System-Contingency-Plan-ISCP.html)

### Cryptography

* [GitLab cryptography standard](/handbook/security/cryptographic-standard.html)
* [Encryption policy](/handbook/security/threat-management/vulnerability-management/encryption-policy.html)

### Data classification

* [Data classification standard](/handbook/security/data-classification-standard.html)
* [Record retention policy](/handbook/legal/record-retention-policy/)
* [Records retention and disposal standard](/handbook/security/records-retention-deletion.html)

### Disaster recovery

* [Disaster recovery plan](https://gitlab.com/gitlab-com/gl-infra/readiness/-/blob/master/library/disaster-recovery/index.md)
* [Database disaster recovery](/handbook/engineering/infrastructure/database/disaster_recovery.html)
* [Database overview](/handbook/engineering/infrastructure/database/)

### Endpoint management

* [Endpoint management at GitLab](/handbook/business-technology/team-member-enablement/onboarding-access-requests/endpoint-management/)
    * [Jamf](/handbook/business-technology/team-member-enablement/onboarding-access-requests/endpoint-management/jamf/)
    * [EDR](/handbook/business-technology/end-user-services/onboarding-access-requests/endpoint-management/edr/)
* [Use Gitleaks as a pre-commit git hook on laptops](/handbook/security/gitleaks.html)

### GitLab.com hardening techniques

* [GitLab projects baseline requirements](/handbook/security/gitlab_projects_baseline_requirements.html)
* [GitLab security requirements for deployment and development](/handbook/security/planning/security-development-deployment-requirements/)

### Incident response and communication

* [Security incident communications plan procedure](/handbook/security/security-operations/sirt/security-incident-communication-plan)
* [Security incident response guide](/handbook/security/security-operations/sirt/sec-incident-response)

### Independent assurance

* [Independent Security Assurance](/handbook/security/security-assurance/field-security/independent_security_assurance.html)

### Logging and monitoring

* [Monitoring of gitlab.com](/handbook/engineering/monitoring/)
* [Log management for gitlab.com](/handbook/engineering/monitoring/#logs)
* [Logging and monitoring architecture](/handbook/engineering/infrastructure/production/architecture/#monitoring-and-logging)
* [GitLab audit logging policy](/handbook/security/audit-logging-policy.html)
* [Log and audit requests process](/handbook/support/workflows/log_requests.html)

### Network security

* [Network security management procedure](/handbook/engineering/infrastructure/network-security/)
* [GitLab security requirements for deployment and development](/handbook/security/planning/security-development-deployment-requirements/)

### Privacy

* [GitLab privacy](/handbook/legal/privacy/)
* [Employee privacy policy](/handbook/legal/privacy/employee-privacy-policy/)
* [Privacy laws and GitLab](/handbook/legal/privacy/privacy-laws.html)
* [California privacy notice](/handbook/legal/privacy/california-privacy-notice.html)
* [Data protection impact assessment (DPIA) policy](/handbook/legal/privacy/dpia-policy/)
*[Account deletion and data access requests workflow](/handbook/support/workflows/account_deletion_access_request_workflows.html)

### Security awareness

* [Security training](/handbook/security/security-training/)
* [Security awareness training program](/handbook/security/security-assurance/governance/sec-awareness-training.html)
* [Security awareness training procedure](/handbook/security/security-assurance/governance/sec-training.html)
* [Phishing program](/handbook/security/security-assurance/governance/phishing.html)

### Third party risk management

* [Security third party risk management](/handbook/security/security-assurance/security-risk/third-party-risk-management.html)

### Threat modeling

* [Threat modeling at GitLab](/handbook/security/threat_modeling/)
* [Threat modeling How To Guide](/handbook/security/threat_modeling/howto.html)
* [Application security threat modeling process](/handbook/security/security-engineering/application-security/runbooks/threat-modeling.html)

### Vulnerability management

* [Vulnerability management standard](/handbook/security/threat-management/vulnerability-management/)
* [Application vulnerability management procedure](/handbook/security/security-engineering/application-security/vulnerability-management.html)
* [Infrastructure vulnerability management procedure](/handbook/security/threat-management/vulnerability-management/Infrastructure-vulnerability-procedure.html)
